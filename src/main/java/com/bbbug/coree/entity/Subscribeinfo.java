package com.bbbug.coree.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Subscribeinfo implements Serializable {
    private static final long serialVersionUID = 6485843061287438106L;
    private Integer mid;
    private Integer user_id;
    private Integer played;
}

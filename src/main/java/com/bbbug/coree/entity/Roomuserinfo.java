package com.bbbug.coree.entity;

import lombok.Data;

import java.io.Serializable;
@Data
public class Roomuserinfo implements Serializable {
    private static final long serialVersionUID = 4952938318185783153L;
    private Integer room_id;
    private Integer room_user;
    private Integer room_online;
    private Integer room_score;
    private String room_name;
    private Integer room_type;
    private String room_notice;
    private Integer room_status;
    private Integer room_public;
    private Integer admin;
    private Integer user_id;
    private String user_name;
    private String user_head;
    private Integer room_addcount;
    private Integer room_addsongcd;
    private Integer room_pushsongcd;
    private Integer room_votepass;
    private Integer room_playone;
}
